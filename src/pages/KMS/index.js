import React, { useState } from "react";
import axios from "axios";

export default function KMS() {

    // ESTADOS ONDE AS VARIÁVIES E VALORES DOS INPUTS FICAM ARMAZENADAS
    const [documento, setDocumento] = useState(null);
    const [tipoAssinante, setTipoAssinante] = useState("Representantes");
    const [tipoDocumento, setTipoDocumento] = useState("XMLDiplomado")
    const [tipo_credencial, setTipoCredencial] = useState("BRYKMS");
    const [tipo_credencial_sistema, setTipoCredencialSistema] = useState("PIN");
    const [valor_credencial, setValorCredencial] = useState("");
    const [uuid_cert, setUuidCert] = useState("");
    const [uuid_pkey, setUUidPkey] = useState("");
    const [user, setUser] = useState("");

    const [loadingAssinatura, setLoadingAssinatura] = useState(false);

    // ESTADOS ONDE AS VARIÁVEIS DE COPIA FICAM ARMAZENADAS
    const [docAcademica, setDocumentoAcademica] = useState(null);
    const [docDiplomado, setDocumentoDiplomado] = useState(null);
    const [loadingCopia, setLoadingCopia] = useState(false);

    // FUNÇÃO QUE SERÁ EXECUTADA AO ENVIAR O FORMULÁRIO DE ASSINATURA
    function handleSubmit(event) {
        // Evita que a página atualize ao dar um submit
        event.preventDefault();

        assina();
    }

    // FUNÇÃO QUE SERÁ EXECUTADA AO ENVIAR O FORMULÁRIO DE COPIA
    async function handleCopy(event) {
        // Evita que a página atualize ao dar um submit
        event.preventDefault();

        const data = new FormData();
        data.append('documentacaoAcademica', docAcademica);
        data.append('XMLDiplomado', docDiplomado);

        setLoadingCopia(true);

        try {
            // REALIZA REQUISIÇÃO PARA O BACKEND
            const response = await axios.post(`http://localhost:3333/XMLDiplomado/copia-nodo`, data);
            console.log("response", response);
            if (response.data.message) {
                alert(response.data.message)
            } else {
                // REALIZA O DOWNLOAD DO DIPLOMA COM A COPIA
                const element = document.createElement("a");
                const file = new Blob([response.data], { type: "text/xml" });
                element.href = URL.createObjectURL(file);
                element.download = "diplomado-com-copia-de-nodo.xml";
                document.body.appendChild(element);
                element.click();
                alert("Ok");
            }

        } catch (err) {
            // DA UM ALERT COM A MENSAGEM DE ERRO
            alert(err);
        }

        setLoadingCopia(false);
    }

    // FUNÇÃO QUE SERÁ CHAMADA PELO FORMULÁRIO PARA ASSINAR O DOCUMENTO
    async function assina() {
        setLoadingAssinatura(true);

        const data = new FormData();
        data.append('documento', documento);
        data.append('tipoAssinatura', tipoAssinante)

        // CREDENCIAIS KMS
        if (!(valor_credencial === "")) { //Pode ser um valor com TOKEN, OTP, PIN
            data.append("valor_credencial", valor_credencial);
        }
        if (!(tipo_credencial === "")) {
            data.append("kms_type", tipo_credencial); //Diz qual tipo de valor entre TOKEN, OTP, PIN
        }
        if (!(tipo_credencial_sistema === "")) {
            data.append("kms_type_credential", tipo_credencial_sistema);
        }
        if (!(uuid_cert === "")) {
            data.append("uuid_cert", uuid_cert);
        }
        if (!(uuid_pkey === "")) {
            data.append("uuid_pkey", uuid_pkey);
        }
        if (!(user === "")) {
            data.append("user", user);
        }

        try {
            // REALIZA REQUISIÇÃO PARA O BACKEND
            const response = await axios.post(`http://localhost:3333/${tipoDocumento}/assinaKms`, data);
            console.log("response", response);
            if (response.data.message) {
                alert(response.data.message)
            } else {
                // REALIZA O DOWNLOAD DO DIPLOMA ASSINADO
                window.location.href = response.data.documentos[0].links[0].href;
            }

        } catch (err) {
            // DA UM ALERT COM A MENSAGEM DE ERRO
            alert(err);
        }
        setLoadingAssinatura(false);
    }

    return (
        <>
            <div className="row justify-content-center align-items-center">
                <div className="col-5" style={{ display: "flex" }}>

                    <form onSubmit={handleSubmit}>
                        <h2>Exemplo assinatura diploma digital com certificado digital em nuvem</h2>
                        <link
                            rel="stylesheet"
                            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
                        />

                        <label htmlFor="tipo_credencial">Tipo da Credencial *</label>
                        <select
                            name="tipo_credencial"
                            value={tipo_credencial}
                            required
                            onChange={event => setTipoCredencial(event.target.value)}
                        >
                            <option value="BRYKMS">BRYKMS</option>
                            <option value="DINAMO">DINAMO</option>
                        </select>

                        <label htmlFor="valor_credencial">Valor da Credencial - Caso seja PIN do KMS ou AToken DINAMO, deve estar em Base64 *</label>
                        <input
                            id="valor_credencial"
                            required
                            placeholder="BRYKMS - PIN/TOKEN/USER | DINAMO - ATOKEN, PIN, OTP"
                            value={valor_credencial}
                            type={tipo_credencial === "BRYKMS" ? ("password") : ("text")}
                            onChange={event => setValorCredencial(event.target.value)}
                        />


                        <label htmlFor="docLabel">Documento a ser assinado</label>
                        <label htmlFor="documento" className="fileUp">
                            {documento ? (
                                <React.Fragment>{documento.name}</React.Fragment>
                            ) : (
                                <React.Fragment>
                                    <i className="fa fa-upload"></i>
                                    Selecione o arquivo
                                </React.Fragment>
                            )}
                            <input
                                id="documento"
                                type="file"
                                onChange={event => setDocumento(event.target.files[0])}
                            />
                        </label>

                        {tipo_credencial !== "DINAMO" ? (<></>) :
                            (<>
                                <label htmlFor="uuid_pkey">ID da Private Key no DINAMO *</label>
                                <input
                                    id="uuid_pkey"
                                    type="text"
                                    placeholder="ID da Private Key no DINAMO"
                                    value={uuid_pkey}
                                    onChange={event => setUUidPkey(event.target.value)}
                                />
                                <label htmlFor="uuid_cert">UUID do Certificado Digital no DINAMO *</label>
                                <input
                                    id="uuid_cert"
                                    type="text"
                                    placeholder="UUID do Certificado Digital no DINAMO"
                                    value={uuid_cert}
                                    onChange={event => setUuidCert(event.target.value)}
                                />
                                <label htmlFor="user">Usuário da conta no DINAMO, caso o valor repassada em Valor Credencial seja um PIN</label>
                                <input
                                    id="user"
                                    type="text"
                                    placeholder="Usuário da conta no DINAMO"
                                    value={user}
                                    onChange={event => setUser(event.target.value)}
                                />
                                <label htmlFor="tipo_credencial_sistema">Tipo da Credencial - DINAMO *</label>
                                <select
                                    name="tipo_credencial_sistema"
                                    value={tipo_credencial_sistema}
                                    required
                                    onChange={event => setTipoCredencialSistema(event.target.value)}
                                >
                                    <option value="PIN">PIN</option>
                                    <option value="TOKEN">TOKEN</option>
                                    <option value="OTP">OTP</option>
                                </select>
                            </>)}

                        {tipo_credencial !== "BRYKMS" ? (<></>) :
                            (<>
                                <label htmlFor="uuid_cert">UUID do Certificado no BRy KMS (forma recomendada de selecionar o certificado)</label>
                                <input
                                    id="uuid_cert"
                                    type="text"
                                    placeholder="UUID do Certificado Digital no BRy KMS"
                                    value={uuid_cert}
                                    onChange={event => setUuidCert(event.target.value)}
                                />
                                <label htmlFor="user">CPF / CNPJ do usuário da conta no BRy KMS (Busca o melhor certificado na conta automaticamente)</label>
                                <input
                                    id="user"
                                    type="text"
                                    placeholder="Usuário da conta no BRy KMS"
                                    value={user}
                                    onChange={event => setUser(event.target.value)}
                                />
                                <label htmlFor="tipo_credencial_sistema">Tipo da Credencial - BRy KMS*</label>
                                <select
                                    name="tipo_credencial_sistema"
                                    value={tipo_credencial_sistema}
                                    required
                                    onChange={event => setTipoCredencialSistema(event.target.value)}
                                >
                                    <option value="PIN">PIN</option>
                                    <option value="TOKEN">TOKEN</option>
                                </select>
                            </>)}


                        <label htmlFor="tipoDocumento">
                            Tipo de Documento *
                        </label>
                        <select
                            id="tipoDocumento"
                            value={tipoDocumento}
                            required
                            onChange={event => { setTipoAssinante("Representantes"); setTipoDocumento(event.target.value) }}
                        >
                            <option value="XMLDiplomado">XML Diplomado</option>
                            <option value="XMLDocumentacaoAcademica">XML Documentação Acadêmica</option>
                        </select>

                        <label htmlFor="tipoAssinante">
                            Tipo de assinante *:{" "}
                        </label>
                        <select
                            id="tipoAssinante"
                            value={tipoAssinante}
                            required
                            onChange={event => setTipoAssinante(event.target.value)}
                        >
                            <option value="Representantes">Representantes</option>
                            {tipoDocumento === "XMLDiplomado" ?
                                (
                                    <>
                                        <option value="IESRegistradora">IES Registradora</option>
                                    </>
                                ) :
                                (
                                    <>
                                        <option value="IESEmissoraDadosDiploma">IES Emissora em Dados Diploma</option>
                                        <option value="IESEmissoraRegistro">IES Emissora para registro</option>
                                    </>
                                )
                            }
                        </select>
                        <button className="btn" type="submit">
                            Assinar
                        </button>
                        <div>{loadingAssinatura ? "Realizando a assinatura do documento..." : ""}</div>
                    </form>
                    <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                    <form onSubmit={handleCopy}>
                        <h2>Exemplo de copia do nodo Dados Diploma</h2>
                        <link
                            rel="stylesheet"
                            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
                        />

                        <label htmlFor="docLabel">Documentação Acadêmica</label>
                        <label htmlFor="docAcademica" className="fileUp">
                            {docAcademica ? (
                                <React.Fragment>{docAcademica.name}</React.Fragment>
                            ) : (
                                <React.Fragment>
                                    <i className="fa fa-upload"></i>
                                    Selecione o arquivo
                                </React.Fragment>
                            )}
                            <input
                                id="docAcademica"
                                type="file"
                                onChange={event => setDocumentoAcademica(event.target.files[0])}
                            />
                        </label>

                        <label htmlFor="docLabel">Documentação Diplomado</label>
                        <label htmlFor="docDiplomado" className="fileUp">
                            {docDiplomado ? (
                                <React.Fragment>{docDiplomado.name}</React.Fragment>
                            ) : (
                                <React.Fragment>
                                    <i className="fa fa-upload"></i>
                                    Selecione o arquivo
                                </React.Fragment>
                            )}
                            <input
                                id="docDiplomado"
                                type="file"
                                onChange={event => setDocumentoDiplomado(event.target.files[0])}
                            />
                        </label>

                        <button className="btn" type="submit">
                            Copiar
                        </button>

                        <div>{loadingCopia ? "Realizando a copia do documento..." : ""}</div>
                    </form>
                </div>
            </div>
        </>
    );
}
